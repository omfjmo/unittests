﻿using BeadandoFeladat2021.MainPrograms;
using NUnit.Framework;
using System;

namespace BeadandoFeladat2021
{
    [TestFixture]
    public class DemeritPointsCalculatorTests
    {
        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(181)]
        public void CalculateDemeritPoints_InvalidSpeedOrAboveMaxSpeedShouldThrowArgumentOutOfRangeException(int speed)
        {
            //Arrange
            DemeritPointsCalculator calculator = new DemeritPointsCalculator();

            //Act, Assert egyben
            Assert.Throws<ArgumentOutOfRangeException>(() => calculator.CalculateDemeritPoints(speed));
        }

        [TestCase(50)]
        [TestCase(49)]
        public void CalculateDemeritPoints_UnderSpeedLimitShouldReturnZero(int speed)
        {
            //Arrange
            int expected = 0;
            DemeritPointsCalculator calculator = new DemeritPointsCalculator();

            //Act
            int actual = calculator.CalculateDemeritPoints(speed);

            //Assert 
            Assert.AreEqual(expected, actual);
        }

        [TestCase(1, 0)]
        [TestCase(51, 0)]
        [TestCase(59, 0)]
        [TestCase(60,1)]
        [TestCase(61,1)]
        [TestCase(69,1)]
        [TestCase(70, 2)]
        [TestCase(71, 2)]
        [TestCase(79, 2)]
        [TestCase(80, 3)]
        [TestCase(81, 3)]
        [TestCase(89, 3)]
        [TestCase(90, 4)]
        [TestCase(91, 4)]
        [TestCase(99, 4)]
        [TestCase(100, 5)]
        [TestCase(101, 5)]
        [TestCase(109, 5)]
        [TestCase(110, 6)]
        [TestCase(111, 6)]
        [TestCase(119, 6)]
        [TestCase(120, 7)]
        [TestCase(121, 7)]
        [TestCase(129, 7)]
        [TestCase(130, 8)]
        [TestCase(131, 8)]
        [TestCase(139, 8)]
        [TestCase(140, 9)]
        [TestCase(141, 9)]
        [TestCase(149, 9)]
        [TestCase(150, 10)]
        [TestCase(151, 10)]
        [TestCase(159, 10)]
        [TestCase(160, 11)]
        [TestCase(161, 11)]
        [TestCase(169, 11)]
        [TestCase(170, 12)]
        [TestCase(171, 12)]
        [TestCase(179, 12)]
        [TestCase(180, 13)]
        public void CalculateDemeritPoints_ShouldReturnCorrectDemeritPoints(int speed, int expected)
        {
            //Arrange
            DemeritPointsCalculator calculator = new DemeritPointsCalculator();

            //Act
            int actual = calculator.CalculateDemeritPoints(speed);

            //Assert 
            Assert.AreEqual(expected, actual);
        }
    }
}
   
