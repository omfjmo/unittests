﻿using BeadandoFeladat2021.MainPrograms;
using NUnit.Framework;

namespace BeadandoFeladat2021
{
    [TestFixture]
    public class FizzBuzzTests
    {
        [TestCase(3)]
        [TestCase(6)]
        public void GetOutput_DivideByThreeShouldReturnFizz(int number)
        {
            //Arrange
            string expected = "Fizz";
            FizzBuzz fizzBuzz = new FizzBuzz();

            //Act
            string actual = fizzBuzz.GetOutput(number);

            //Assert 
            Assert.AreEqual(expected, actual);
        }

        [TestCase(5)]
        [TestCase(10)]
        public void GetOutput_DivideByFiveShouldReturnBuzz(int number)
        {
            //Arrange
            string expected = "Buzz";
            FizzBuzz fizzBuzz = new FizzBuzz();

            //Act
            string actual = fizzBuzz.GetOutput(number);

            //Assert 
            Assert.AreEqual(expected, actual);
        }

        [TestCase(0)]
        [TestCase(15)]
        [TestCase(45)]
        public void GetOutput_DivideByThreeAndFiveShouldReturnFizzBuzz(int number)
        {
            //Arrange
            string expected = "FizzBuzz";
            FizzBuzz fizzBuzz = new FizzBuzz();

            //Act
            string actual = fizzBuzz.GetOutput(number);

            //Assert 
            Assert.AreEqual(expected, actual);
        }

        [TestCase(-1)]
        [TestCase(1)]
        [TestCase(2)]
        public void GetOutput_ShouldReturnTheInput(int number)
        {
            //Arrange
            string expected = number.ToString();
            FizzBuzz fizzBuzz = new FizzBuzz();

            //Act
            string actual = fizzBuzz.GetOutput(number);

            //Assert 
            Assert.AreEqual(expected, actual);
        }
    }
}
