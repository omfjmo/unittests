﻿using BeadandoFeladat2021.MainPrograms;
using NUnit.Framework;

namespace BeadandoFeladat2021.Tests
{
    [TestFixture]
    public class LeapYearTests
    {
        [TestCase(1976, true)]
        [TestCase(2020, true)]
        [TestCase(2000, true)]
        [TestCase(1700, true)]
        public void checkLeapYear_ShouldReturnTrue(int year, bool expected)
        {
            //Arrange
            LeapYear leapyear = new LeapYear();

            //Act
            bool actual = leapyear.checkLeapYear(year);

            //Assert 
            Assert.AreEqual(expected, actual);
        }

        [TestCase(2022, false)]
        [TestCase(2001, false)]
        public void checkLeapYear_ShouldReturnFalse(int year, bool expected)
        {
            //Arrange
            LeapYear leapyear = new LeapYear();

            //Act
            bool actual = leapyear.checkLeapYear(year);

            //Assert 
            Assert.AreEqual(expected, actual);
        }
    }
}
