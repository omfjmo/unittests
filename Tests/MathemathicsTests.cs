﻿using BeadandoFeladat2021.MainPrograms;
using NUnit.Framework;
using System.Collections.Generic;

namespace BeadandoFeladat2021.Tests
{
    [TestFixture]
    public class MathemathicsTests
    {
        [TestCase(5, 6, 11)]
        [TestCase(0, 8, 8)]
        [TestCase(-1, 2, 1)]
        [TestCase(-1, -1, -2)]
        public void Add_ShouldWork(int x, int y, int expected)
        {
            //Arrange
            Mathemathics mathemathics = new Mathemathics();

            //Act
            int actual = mathemathics.Add(x, y);

            //Assert 
            Assert.AreEqual(expected, actual);
        }

        [TestCase(1_073_741_824, 1_073_741_824, 2_147_483_648)]
        [TestCase(-1_073_741_825, -1_073_741_825, -2_147_483_650)]
        public void Add_ShouldHandleIntegerValueOverflow(int x, int y, int expected)
        {
            //Arrange
            Mathemathics mathemathics = new Mathemathics();

            //Act
            int actual = mathemathics.Add(x, y);

            //Assert 
            Assert.AreEqual(expected, actual);
        }

        [TestCase(-1, 2, 2)]
        [TestCase(0, 15, 15)]
        [TestCase(8, 2, 8)]
        [TestCase(-8, -2, -2)]
        [TestCase(-8, -8, -8)]
        public void Max_ShouldWork(int x, int y, int expected)
        {
            //Arrange
            Mathemathics mathemathics = new Mathemathics();

            //Act
            int actual = mathemathics.Max(x, y);

            //Assert 
            Assert.AreEqual(expected, actual);
        }

        [TestCase(1)]
        [TestCase(2)] 
        public void GetOddNumbers_OneItemToListShouldWork(int limit)
        {
            //Arrange
            int[] expected = { 1 };

            Mathemathics mathemathics = new Mathemathics();

            //Act
            IEnumerable<int> actual = mathemathics.GetOddNumbers(limit);

            //Assert 
            Assert.AreEqual(expected, actual);
        }

        [TestCase(10)]
        public void GetOddNumbers_MoreItemsToListShouldWork(int limit)
        {
            //Arrange
            int[] expected = { 1, 3, 5, 7, 9 };
           
            Mathemathics mathemathics = new Mathemathics();

            //Act
            IEnumerable<int> actual = mathemathics.GetOddNumbers(limit);

            //Assert 
            Assert.AreEqual(expected, actual);
        }

        [TestCase(0)]
        public void GetOddNumbers_ShouldReturnEmptyList(int limit)
        {
            //Arrange
            int[] expected = {  };

            Mathemathics mathemathics = new Mathemathics();

            //Act
            IEnumerable<int> actual = mathemathics.GetOddNumbers(limit);

            //Assert 
            Assert.AreEqual(expected, actual);
        }
    }
}
