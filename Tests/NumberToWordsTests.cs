﻿using BeadandoFeladat2021.MainPrograms;
using NUnit.Framework;

namespace BeadandoFeladat2021.Tests
{
    [TestFixture]
    public class NumberToWordsTests
    {
        [TestCase(0)]
        public void convertNumbers_ShouldReturnZero(int number)
        {
            //Arrange
            string expected = "zero";

            //Act
           string actual = NumberToWords.convertNumbers(number);

            //Assert 
            Assert.AreEqual(expected, actual);
        }

        [TestCase(-99)]
        public void convertNumbers_ShouldReturnMinusAndConvertedNumber(int number)
        {
            //Arrange
            string expected = "minus ninety-nine";

            //Act
            string actual = NumberToWords.convertNumbers(number);

            //Assert 
            Assert.AreEqual(expected, actual);
        }

        [TestCase(2_000_000)]
        public void convertNumbers_ShouldReturnMillion(int number)
        {
            //Arrange
            string expected = "two million";

            //Act
            string actual = NumberToWords.convertNumbers(number);

            //Assert 
            Assert.AreEqual(expected, actual);
        }

        [TestCase(5000)]
        public void convertNumbers_ShouldReturnThousand(int number)
        {
            //Arrange
            string expected = "five thousand";

            //Act
            string actual = NumberToWords.convertNumbers(number);

            //Assert 
            Assert.AreEqual(expected, actual);
        }

        [TestCase(100)]
        public void convertNumbers_ShouldReturnHundred(int number)
        {
            //Arrange
            string expected = "one hundred";

            //Act
            string actual = NumberToWords.convertNumbers(number);

            //Assert 
            Assert.AreEqual(expected, actual);
        }

        [TestCase(1, "one")]
        [TestCase(11, "eleven")]
        [TestCase(12, "twelve")]
        [TestCase(18, "eighteen")]
        [TestCase(19, "nineteen")]
        [TestCase(20, "twenty")]
        [TestCase(21, "twenty-one")]
        [TestCase(40, "forty")]
        [TestCase(45, "fourty-five")]
        [TestCase(178, "one hundred and seventy-eight")]
        [TestCase(2100, "two thousand and one hundred")]
        [TestCase(56_340, "fifty-six thousand and three hundred and forty")]
        [TestCase(1_576_897, "one million, five hundred and seventy-six thousand and eight hundred and ninety-seven")]
        public void convertNumbers_ShouldWork(int number, string expected)
        {
            //Arrange, Act egyben
            string actual = NumberToWords.convertNumbers(number);

            //Assert 
            Assert.AreEqual(expected, actual);
        }
    }

}
